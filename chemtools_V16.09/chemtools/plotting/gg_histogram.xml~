<tool id="ggHistogram" name="Histogram" version="1.0.1">
  <description> Histogram with ggplot</description>
  <requirements>
    <requirement type="package" version="2.11.0">R</requirement>
  </requirements>
  <command interpreter="bash">r_wrapper.sh $script_file</command>

  <inputs>
    <param name="input" type="data" format="tabular" label="Dataset"/>
    <param name="xcol" type="data_column" data_ref="input" label="Column for x axis" numerical="true"  multiple="False" use_header_names="true" force_select="true" />
    <param name="main" type="text" value="Histogram plot" label="Plot Title"/>
    <param name="bw"  size="5" value="1"  type="float"  label="Bin Width "/>
    <conditional name="factorm">
      <param format="text" name="fact" type="select" label="use a column of a dataset as bar color">
        <option value="yes" >yes</option>
        <option value="no" selected="true">no</option>
      </param>
      <when value="yes">
        <param name="input_col" type="data" format="tabular" label="Dataset"/>
        <param name="ym" type="data_column" data_ref="input_col" label="Column factor choice color bar" numerical="false"  multiple="False" use_header_names="true" force_select="true" />
      </when>
    </conditional>
  </inputs>

  <configfiles>
    <configfile name="script_file">
      ## Setup R error handling to go to stderr
      options( show.error.messages=F,
               error = function () { cat( geterrmessage(), file=stderr() ); q( "no", 1, F ) } )
      library(ggplot2)
        s = read.table( "${input.file_name}",h=T,sep="\t",strip.white=T)
      ## Histogram for axis / labels
      name=names(s);
      #if str($factorm.fact) == 'no'
      p=ggplot(s[,${xcol},drop=F], aes_string(x=name[${xcol}])) + geom_histogram(position="stack", alpha=0.5,binwidth = ${bw}) + ggtitle("${main}")
      #elif str($factorm.fact) == 'yes'
      scol = read.table( "${factorm.input_col.file_name}",h=T,sep="\t")
      df=data.frame(s[,${xcol},drop=F],scol[,${factorm.ym},drop=F])
      name=names(df);
      eval(parse(text=(paste0("df$",name[2],"=as.factor(df$",name[2],")"))))
      p=ggplot(df, aes_string(x=name[1],fill=name[2], group=name[2])) + geom_histogram(position="stack", alpha=0.5,binwidth = ${bw}) + ggtitle("${main}")
      #end if
      ## Save in PDF file
      ggsave("${histo_plot}", plot = p, device = "pdf",width=5.57, height=5.24)
    </configfile>
  </configfiles>

  <outputs>
    <data format="pdf" name="histo_plot" label="histogram on ${input.name}(${xcol})" />
  </outputs>

    <tests>
        <test>

        </test>
    </tests>
<help>
.. class:: warningmark

**TIP:** If your data is not TAB delimited, use *Convert format data Tools-&gt;Convert*

.. class:: warningmark

**TIP:** If the graph don't display when you click on eye button, click on view details button and retry click on eye button.

.. class:: infomark

**Authors** Fabien Gogé (IRSTEA), Virginie Rossard (INRA), Eric Latrille (INRA), Jean-Michel Roger (IRSTEA), Jean-Claude Boulet (INRA)



---------------------------------------------------

=========
HISTOGRAM
=========

-----------
Description
-----------


Create a histogram

-----------
Input files
-----------

Matrix of numeric data with headers (tabular format) and decimal separator ".".


+--------+-------+--------+
| Name   | Carbon| etc    |
+========+=======+========+
|M102T645| 2.10  | etc    |
+--------+-------+--------+
|M105T604| 50.4  | etc    |
+--------+-------+--------+

----------
Parameters
----------
**Column for x axis**

Choice of column name(s) to plot histogram

**Bin Width**

**use a column of a dataset as bar color**

if 'yes': attribute a color bar for each factor levels.

  **Column factor choice for color bar**

  choice of the column name containing the factor variable

------------
Output files
------------

**histogram on input.filename**:a file .pdf

.. image:: ./static/images/chemflow/gghisto_tool_example.png
         :height: 250
         :width: 300

</help>
</tool>
