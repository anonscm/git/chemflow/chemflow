#!/bin/sh

### Run octave providing the octave script in $1 as standard input and passing
### the remaining arguments on the command line

# Function that writes a message to stderr and exits
fail()
{
    echo "$@" >&2
    exit 1
}

# Ensure scilab executable is found
which octave > /dev/null || fail "'octave' is required by this tool but was not found on path"

# Extract first argument
infile=$1; shift

# Ensure the file exists
test -f $infile || fail "octave input file '$infile' does not exist"

# Invoke scilab passing file named by first argument to stdin
octave -q $* < $infile
