<tool id="knnda_bash" name="KNNDA" version="0.0.1">
  <description> K-Nearest-Neighbor Discriminant Analysis </description>
  <requirements>
    <requirement type="package" version="5.0.0">Scilab</requirement>
  </requirements>

  <command interpreter="bash">scilab_wrapper.sh $script_file</command>

  <inputs>
    <param name="Xdata"  format="tabular"       type="data"     label="Select X data" help="Dataset (n x p) containing the n spectra of p variables."/>
    <param name="Yclass"  format="tabular"      type="data"     label="Select y class" help="Dataset (n x 1) containing the class values."        />
    <param name="ycolumn" type="data_column"     data_ref="Yclass"  numerical="True"  multiple="False" use_header_names="true" force_select="true" label="Column of y class chosen for the calculation"/>
    <param name="crossval"   size="5" value="4"  type="integer"  min="2" max="200" label="Number of blocs for cross-validation" />
    <param name="knn"  size="5" value="10"  type="integer"  min="2" max="50" label="Number of Nearest-Neighbor" />
    <param format="text" name="export_tables" type="select" label="export confusion tables (CAL and CV)">
      <option value="yes">yes</option>
      <option value="no" selected="true">no</option>
    </param>
  </inputs>

  <configfiles>
    <configfile name="script_file">
      f=atomsLoad(["FACT"]);
      stacksize('max');

      exec(getenv('PATH_CHEM')+'/scilab/knnda.sci', -1);
      exec(getenv('PATH_CHEM')+'/scilab/cvclass2.sci', -1);
      exec(getenv('PATH_CHEM')+'/scilab/classknnda.sci', -1);
      exec(getenv('PATH_CHEM')+'/scilab/classtestknnda.sci', -1);

      //initialisation aleatoire de la graine
      n=getdate()
      rand('seed',n(10))

      x=glx_tab2div("${Xdata.file_name}");
      y=glx_tab2div("${Yclass.file_name}");

      ycol=[${ycolumn}]-1;
      res_da=knnda(x,y.d(:,ycol),${crossval},${knn},'s');

      #if str($export_tables)== 'yes'
        kill=div2tab(res_da.conf_cal_nobs,"${conf_cal}");
        kill=div2tab(res_da.conf_cv,"${conf_cv}");
      #end if

      res_da.err_cal.i="K="+string(${knn});
      res_da.err_cal.v(1)="error of classification, p.cent";
      res_da.err_cal.v(2)="error of cross-validation, p.cent";
      kill=div2tab(res_da.err_cal,"${error_percent}");


      // kill=glx_resreg2xml(res_da,"${model}");
      //div2mat(res_da,"${model}",'res_da');
      save("${model}",'res_da');
    </configfile>
  </configfiles>

  <outputs>
  <data format="input"   name="error_percent"         type="data"     label="K${knn}NDA on ($Xdata.name;${Yclass.name}(c${ycolumn.value_label})):error percent"    />
  <data format="mat"   name="model"         type="data"     label="K${knn}NDA on ($Xdata.name;${Yclass.name}(c${ycolumn.value_label})):model" />
  <data format="input"   name="conf_cal"         type="data"     label="K${knn}NDA on ($Xdata.name;${Yclass.name}(c${ycolumn.value_label})):confusion_CAL"    >
    <filter>export_tables == "yes"</filter>
  </data>
  <data format="input"   name="conf_cv"         type="data"     label="K${knn}NDA on ($Xdata.name;${Yclass.name}(c${ycolumn.value_label})):confusion_CV"    >
     <filter>export_tables == "yes"</filter>
  </data>
  </outputs>

  <tests>
    <test>
      <param name="Xdata" value="Xnir.tabular"/>
      <param name="Yclass" value="Yclass.tabular"/>
      <param name="ycolumn" value="2" />
      <param name="crossval" value="4" />
      <param name="latentvar" value="10" />
      <param name="distance" value="0" />
      <output name="conf_cal" file="conf_cal.tabular"/>
      <output name="conf_cv" file="conf_cv.tabular"/>
      <output name="perc_err" file="perc_err.tabular"/>
      <output name="model" file="model.xml"/>
    </test>
  </tests>
<help>

.. class:: warningmark

**TIP:** If your data is not TAB delimited, use *Convert format data Tools-&gt;Convert*

.. class:: infomark

**Authors** Fabien Gogé (IRSTEA), Virginie Rossard (INRA), Eric Latrille (INRA), Jean-Michel Roger (IRSTEA), Jean-Claude Boulet (INRA)


---------------------------------------------------

=====
KNNDA
=====

-----------
Description
-----------

Performs a **K-Nearest-Neighbor Discriminant Analysis**. Missing values are not permitted.
This scripts uses the FACT-version module.


-----------
Input files
-----------

**X data**
Matrix of numeric data with headers (tabular format) and decimal separator ".".


+--------+-------+--------+
| Name   | 400   | 402    |
+========+=======+========+
|M102T645| 0.63  |0.64    |
+--------+-------+--------+
|M105T604| 0.5   |0.49    |
+--------+-------+--------+



**y class**
Matrix of factor data with headers (tabular format)

+--------+-------+--------+
| Name   | Origin| etc    |
+========+=======+========+
|M102T645|  1    | etc    |
+--------+-------+--------+
|M105T604|  2    | etc    |
+--------+-------+--------+


----------
Parameters
----------

**Column of y class for the calculation**

Choice of property to predict

**Number of blocs for cross-validation**

Choice of the number of block for cross-validation

**Number of Nearest-Neighbor**

Choice of the number of Nearest-Neighbor


**export confusion tables (CAL and CV)**

    - if 'yes': the calibration and cross-validation confusion tables (CAL and CV)**


------------
Output files
------------

Outfile1:**KNNDA on (Xdata.file_name;ydata.file_name(cN°)):error percent**

dataset containing the error percentage in calibration and cross validation for each Nearest-Neighbor

Outfile2:**KNNDA on (Xdata.file_name;ydata.file_name(cN°)):model**

mat file containing all results of model

if **export confusion tables (CAL and CV)** ='yes'

Outfile3:**KNNDA on (Xdata.file_name;ydata.file_name(cN°)):confusion_CAL**

dataset containing calibration confusion matrix

Outfile4:**KNNDA on (Xdata.file_name;ydata.file_name(cN°)):confusion_CV**

dataset containing cross-validation confusion matrix

</help>
</tool>
