<tool id="distancem" name="matrix distance" version="0.0.1">
  <description>  compute matrix distance </description>
  <requirements>
    <requirement type="package" version="5.0.0">scilab</requirement>
  </requirements>

  <command interpreter="bash">r_wrapper.sh $script_file</command>

  <inputs>
    <param name="Xdata"  format="tabular"  type="data" label="Select X data" help="Dataset (n x p) containing the n spectra of p variables."/>
    <param format="text" name="dist" type="select" label="distance choice">
      <option value="euclid" selected="True">Euclidian</option>
      <option value="max">Maximum</option>
      <option value="man">Manhattan</option>
      <option value="canb">Canberra</option>
      <option value="bin">Binary</option>
    </param>
  </inputs>

  <configfiles>
    <configfile name="script_file">
      ## Setup R error handling to go to stderr
      options( show.error.messages=F,
               error = function () { cat( geterrmessage(), file=stderr() ); q( "no", 1, F ) } )
      x = read.table( "${Xdata.file_name}",h=T,sep="\t")
      if(length(which(duplicated(x[,1])==T))>0) stop(paste(as.character(x[which(duplicated(x[,1])==T),1]),"replicated at line",which(duplicated(x[,1])==T),"  ")) else x = read.table("${Xdata.file_name}",h=T,sep="\t",row.names=1, strip.white=T)
      row.names(x)=gsub("^\\s+|\\s+$", "", row.names(x))

      #if str($dist) == "euclid"
      D2 = dist(x)
      D2=as.matrix(D2)
      D2=round(D2,digits=2)
      #elif str($dist) == "max"
      D2 = dist(x,method="maximum")
      D2=as.matrix(D2)
      D2=round(D2,digits=2)
      #elif str($dist) == "man"
      D2 = dist(x,method="manhattan")
      D2=as.matrix(D2)
      D2=round(D2,digits=2)
      #elif str($dist) == "canb"
      D2 = dist(x,method="canberra")
      D2=as.matrix(D2)
      D2=round(D2,digits=2)
      #elif str($dist) == "bin"
      D2 = dist(x,method="binary")
      D2=as.matrix(D2)
      D2=round(D2,digits=2)
      #end if
      write.table(D2, file = "${MD}", sep = "\t",quote=F,row.names = TRUE ,col.names = NA)
    </configfile>
  </configfiles>

  <outputs>
    <data name="MD" format="input" type="data" label="matrix ${dist.value_label}(${Xdata.name})" />
  </outputs>
  <tests>
    <test>

    </test>
  </tests>
<help>

.. class:: warningmark

**TIP:** If your data is not TAB delimited, use *Convert format data Tools-&gt;Convert*


**Authors** Fabien Gogé (IRSTEA), Virginie Rossard (INRA), Eric Latrille (INRA), Jean-Michel Roger (IRSTEA), Jean-Claude Boulet (INRA)


---------------------------------------------------

===============
MATRIX DISTANCE
===============


-----------
Description
-----------

computes distance matrix computed by using the specified distance measure to compute the distances between the rows of a data matrix.

-----------
Input files
-----------

Matrix of numeric data with headers (tabular format) and decimal separator ".".


+--------+-------+--------+
| Name   | 400   | 402    |
+========+=======+========+
|M102T645| 0.63  |0.64    |
+--------+-------+--------+
|M105T604| 0.5   |0.49    |
+--------+-------+--------+

----------
Parameters
----------

**distance choice**

  - **Euclidean**
    * *Usual distance between the two vectors (2 norm aka L_2), sqrt(sum((x_i - y_i)^2)).*

  - **Maximum**
    * *Maximum distance between two components of x and y (supremum norm)*

  - **Manhattan**
    * *Absolute distance between the two vectors (1 norm aka L_1).*

  - **Canberra**
    * *sum(|x_i - y_i| / |x_i + y_i|). Terms with zero numerator and denominator are omitted from the sum and treated as if the values were missing.*

    * *This is intended for non-negative values (e.g., counts): taking the absolute value of the denominator is a 1998 R modification to avoid negative distances.*

  - **Binary**
    * *(aka asymmetric binary): The vectors are regarded as binary bits, so non-zero elements are ‘on’ and zero elements are ‘off’. The distance is the proportion of bits in which only one is on amongst those in which at least one is on.*

------------
Output files
------------

**distance_choice(input.file_name)**


</help>
</tool>
