<tool id="plsda_bash" name="PLS-DA" version="0.0.1">
  <description> Partial Least Square Discriminant Analysis </description>
  <requirements>
    <requirement type="package" version="5.0.0">Scilab</requirement>
  </requirements>

  <command interpreter="bash">scilab_wrapper.sh $script_file</command>

  <inputs>
    <param name="Xdata"  format="tabular"           type="data"     label="Select X data" help="Dataset (n x p) containing the n spectra of p variables."/>
    <param name="Yclass"  format="tabular"           type="data"     label="Select y class" help="Dataset (n x q) containing the class values."        />
    <param name="ycolumn" type="data_column"     data_ref="Yclass"  numerical="True"  multiple="false" use_header_names="true" force_select="true" label="Column of y class chosen for the calculation"/>
    <param name="crossval"   size="5" value="4"  type="integer"  min="2" max="200" label="Number of blocs for cross-validation"                                         />
    <param name="latentvar"  size="5" value="10"  type="integer"  min="2" max="50" label="Number of discriminant variables(DV)"                                                   />
    <param format="text" name="distance" type="select" label="distance choice">
      <option value="0" selected="true">Mahalanobis</option>
      <option value="1">Euclidian</option>
    </param>
    <param format="text" name="export_tables" type="select" label="export confusion tables (CAL) for each discriminant variables (DV) models">
      <option value="yes">yes</option>
      <option value="no" selected="true">no</option>
    </param>
  </inputs>

  <configfiles>
    <configfile name="script_file">
      f=atomsLoad(["FACT"]);
      stacksize('max');
      exec(getenv('PATH_CHEM')+'/scilab/classtestxx.sci', -1);
      exec(getenv('PATH_CHEM')+'/scilab/cvclass.sci', -1);

      //initialisation aleatoire de la graine
      n=getdate()
      rand('seed',n(10))


      x=glx_tab2div("${Xdata.file_name}");
      y=glx_tab2div("${Yclass.file_name}");

      ycol=[${ycolumn}]-1;
      res_da=plsda(x,y(:,ycol),${crossval},${latentvar},${distance});

      kill=div2tab(res_da.err,"${error_percent}");
      kill=div2tab(res_da.scores,"${scores_da}");

      #if str($export_tables)== 'yes'
          mkdir tables_plsda;
          for i=1:size(res_da.conf_cal_nobs)
          execstr("kill=div2tab(res_da.conf_cal_nobs("+string(i)+"),''"+"tables_plsda/confusion_cal"+string(i)+"DV"+"'');");
      end
      #end if


      // kill=glx_resreg2xml(res_da,"${model}");
      //div2mat(res_da,"${model}",'res_da');
      save("${model}",'res_da');

    </configfile>
  </configfiles>

  <outputs>
  <data format="input"   name="error_percent"         type="data"     label="PLS-DA on ($Xdata.name;${Yclass.name}(c${ycolumn.value_label})):error percent"    />
  <data format="input"   name="scores_da"         type="data"     label="PLS-DA on ($Xdata.name;${Yclass.name}(c${ycolumn.value_label})):scores"    />
  <data format="txt" name="report">
     <filter>export_tables == "yes"</filter>
     <discover_datasets pattern="__designation__" ext="tabular" directory="tables_plsda" visible="true" />
  </data>
  <data format="mat"   name="model"         type="data"     label="PLS-DA on ($Xdata.name;${Yclass.name}(c${ycolumn.value_label})):model" />
  </outputs>
  <tests>
    <test>
      <param name="Xdata" value="Xnir.tabular"/>
      <param name="Yclass" value="Ynir.tabular"/>
      <param name="ycolumn" value="2" />
      <param name="crossval" value="4" />
      <param name="latentvar" value="10" />
      <output name="rmsec_secv" file="rmsec-rmsecv.tabular"/>
      <output name="b_coeffs" file="b-coeffs.tabular"/>
      <output name="ypredcv" file="ypredcv.tabular"/>
      <output name="model" file="model.xml"/>
    </test>
  </tests>
<help>

.. class:: warningmark

**TIP:** If your data is not TAB delimited, use *Convert format data Tools-&gt;Convert*

.. class:: infomark

**Authors** Fabien Gogé (IRSTEA), Virginie Rossard (INRA), Eric Latrille (INRA), Jean-Michel Roger (IRSTEA), Jean-Claude Boulet (INRA)


---------------------------------------------------

======
PLS-DA
======

-----------
Description
-----------

Performs a **Partial Least Square Discriminant Analysis**. Missing values are not permitted.
This scripts uses the FACT-version module.

-----------
Input files
-----------

**X data**
Matrix of numeric data with headers (tabular format) and decimal separator ".".


+--------+-------+--------+
| Name   | 400   | 402    |
+========+=======+========+
|M102T645| 0.63  |0.64    |
+--------+-------+--------+
|M105T604| 0.5   |0.49    |
+--------+-------+--------+



**y class**
Matrix of factor data with headers (tabular format)

+--------+-------+--------+
| Name   | Origin| etc    |
+========+=======+========+
|M102T645|  1    | etc    |
+--------+-------+--------+
|M105T604|  2    | etc    |
+--------+-------+--------+


----------
Parameters
----------

**Column of y class chosen for the calculation**

Choice of property to predict

**Number of blocs for cross-validation**

Choice of the number of block for cross-validation

**Number of discriminant variables (DV)**

Choice of the number of discriminant variables

**distance choice**

specifying the metric to be used for calculating dissimilarities between observations.

  -'Mahalanobis'

  -'Euclidian'

**export confusion tables (CAL) for each discriminant variables (DV) models**

    - if 'yes': the calibration confusion tables (CAL) for each discriminant variables (DV) models**

------------
Output files
------------

Outfile1:**PLS-DA on (Xdata.file_name;yclass.file_name(cN°)):error percent**

dataset containing the error percentage in calibration and cross validation for each DV

Outfile2:**PLS-DA on (Xdata.file_name;yclass.file_name(cN°)):scores**

dataset containing the scores fro each DV

Outfile3:**PLS-DA on (Xdata.file_name;yclass.file_name(cN°)):model**

mat file containing all results of model

if **export confusion tables (CAL) for each discriminant variables (DV) models** ='yes'

Outfile4:**PLS-DA on data i and data j (confusion_calnDV)**

dataset containing calibration confusion matrix for 1 to n DV




</help>
</tool>
