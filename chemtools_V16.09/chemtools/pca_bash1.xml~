<tool id="pca_bash1" name="PCA" version="0.0.1">
  <description> Principal Component Analysis </description>
  <requirements>
    <requirement type="package" version="5.0.0">Scilab</requirement>
  </requirements>
  <command interpreter="bash">scilab_wrapper.sh $script_file</command>
  <inputs>
    <param name="Xdata"   format="tabular"  type="data" label="Select X data" help="Dataset (n x p) containing the n spectra of p variables."/>
    <param format="text" name="centering" type="select" label="Centering option">
      <option value="1" selected="true">yes</option>
      <option value="0">no</option>
    </param>
    <param format="text" name="scaling" type="select" label="Scaling option">
      <option value="1" >yes</option>
      <option value="0" selected="true">no</option>
    </param>
  <param name="latentvar"   size="5"      type="integer" value="20" label="Number of PC max"  help="Choice of the principal component displayed"/>
  <param format="text" name="outlier" type="select" label="compute outliers statistics">
    <option value="yes">yes</option>
    <option value="no" selected="True" >no</option>
  </param>
  </inputs>

  <configfiles>
    <configfile name="script_file">
      f=atomsLoad(["FACT"]);
      stacksize('max');
      x=glx_tab2div("${Xdata.file_name}");
      exec(getenv('PATH_CHEM')+'/scilab/glx_pcana.sci', -1)
      exec(getenv('PATH_CHEM')+'/scilab/outlier.sci', -1)
      res=glx_pcana(x,{$centering},${scaling},${latentvar});

      kill=div2tab(res.scores,"${scores}");
      kill=div2tab(res.eigenvec,"${loadings}");
      kill=div2tab(res.ev_pcent,"${var_explained}");
      kill=glx_resreg2xml(res,"${model}");
      div2mat(res,'res',"${model}");


      #if str($outlier) == "yes"
          #if str($centering)=="1"
          [t2,res,lev]=outlier(centering(x),res.scores,res.eigenvec,${latentvar});
          #elif str($centering)=="0"
          [t2,res,lev]=outlier(x,res.scores,res.eigenvec,${latentvar});
          #end if
      kill=div2tab(t2,"${T2}");
      kill=div2tab(res,"${res}");
      #end if


      //write(0,'Scilab stderr (Fortran');


    </configfile>
  </configfiles>

  <outputs>
    <data format="input"   name="scores"     label="PCA scores:${Xdata.name}"           type="data"   />
    <data format="input"   name="loadings"     label="PCA eigenvectors:${Xdata.name}"         type="data"  />
    <data format="input"   name="var_explained"     label="PCA explained variance(%):${Xdata.name}"  type="data"  />
    <data format="mat"   name="model"         type="data"     label="PCA model:${Xdata.name}"    />
    <data format="input"   name="T2"      type="data"    label="PCA T2:${Xdata.name}"  >
        <filter>outlier == "yes"</filter>
    </data>
    <data format="input"   name="res"      type="data"    label="PCA Q:${Xdata.name}"  >
        <filter>outlier == "yes"</filter>
    </data>
  </outputs>
  <tests>
    <test>
      <param name="Xdata" value="Xnir.tabular"/>
      <param name="centering" value="1"/>
      <param name="scaling" value="0"/>
      <param name="latentvar" value="20" />
      <output name="scores" file="PCA_scores.tabular"/>
      <output name="loadings" file="PCA_eigenvectors.tabular"/>
      <output name="var_explained" file="PCA_explained_variance.tabular"/>
    </test>
  </tests>
  <help>

.. class:: warningmark

**TIP:** If your data is not TAB delimited, use *Convert format data Tools-&gt;Convert*

.. class:: infomark

**Authors** Fabien Gogé (IRSTEA), Virginie Rossard (INRA), Eric Latrille (INRA), Jean-Michel Roger (IRSTEA), Jean-Claude Boulet (INRA)


---------------------------------------------------

===
PCA
===

-----------
Description
-----------


Performs **Principal Component Analysis (PCA)** using singular vector decomposition. This scripts uses the FACT module.


-----------
Input files
-----------

Matrix of numeric data with headers (tabular format) and decimal separator ".".


+--------+-------+--------+
| Name   | 400   | 402    |
+========+=======+========+
|M102T645| 0.63  |0.64    |
+--------+-------+--------+
|M105T604| 0.5   |0.49    |
+--------+-------+--------+

----------
Parameters
----------

**centering**

If 'no': no centering of the variables

**scaling**

If 'no': no scaling of the variables

**Number of PC max**

Number of principal components

**compute outliers statistics**

If 'yes': compute T2 Hotelling and residuals models


------------
Output files
------------



Outfile1:**PCA scores:input.file_name**

dataset containing the PCA scores from 1 to number of PC max

Outfile2:**PCA eigenvectors:input.file_name**

dataset containing the PCA eigenvectors from 1 to number of PC max

Outfile3:**PCA explained variance(%):input.file_name**

dataset containing the PCA explained variance in percent from 1 to number of PC max

Outfile4:**PCA model:input.file_name**

mat file containing all the results of pca

if **compute outliers statistics** ="yes"

Outfile5:**PCA T2:input.file_name**

dataset containing the values of Hotelling statistics

Outfile6:**PCA Q:input.file_name**

dataset containing the values of residuals of model


  </help>
</tool>
