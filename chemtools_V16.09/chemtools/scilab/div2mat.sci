function div2mat(xinput,varname,filename)

    // xinput: structure Div ou structure comprenenant comme éléments des structuress ou des fichiers classiques
    // varname: - partie du nom du fichier de sortie, qui sera: 'varname'.mat
    //          - également nom de la structure qui est sauvegardée dans le .mat
    // JCB 19/02/16

    //filename=varname +'.mat';

    if typeof(xinput)=='div' then   // xdiv est bien une structure Div
        xoutput=[];
        //xoutput.description = ['div  ';'d    ';'i    ';'v    ';'infos'];
        xoutput.d = xinput(2);
        xoutput.i = justify(xinput(3),'l');
        xoutput.v = justify(xinput(4),'l');


    elseif typeof(xinput)=='st' then
            fields_names = fieldnames(xinput);
            nfields=size(fields_names,1);
            xoutput=[];
            for i=1:nfields;
                field_i=fields_names(i);           // on récupère le nom du champ
                field_i=strsubst(field_i,' ','');  // on enlève tous les espaces
                execstr(msprintf('field_type=typeof(xinput.%s);',field_i));
                if field_type=='div' then
                    execstr(msprintf('xoutput.%s.d=[]; xoutput.%s.d=xinput.%s.d',field_i,field_i,field_i));
                    execstr(msprintf('xoutput.%s.i=[];',field_i));
                    execstr(msprintf('xoutput.%s.i=justify(xinput.%s.i,''l'');',field_i,field_i));
                    execstr(msprintf('xoutput.%s.v=[];',field_i));
                    execstr(msprintf('xoutput.%s.v=justify(xinput.%s.v,''l'');',field_i,field_i));
                elseif field_type=='list' then
                    execstr(msprintf('fj_length=max(size(xinput.%s));',field_i));
                    disp(fj_length,'fj_length')
                    for j=1:fj_length;
                        disp(j,'j=')
                         execstr(msprintf('fj_type=typeof(xinput.%s(j));',field_i));
                         disp(fj_type,'fj_type=')
                         if fj_type=='div' then
                             disp('passe ici')
                            execstr(msprintf('xoutput.%s.l%s.d=[]; xoutput.%s.l%s.d=xinput.%s(j).d',field_i,string(j),field_i,string(j),field_i));
                            execstr(msprintf('xoutput.%s.l%s.i=[];',field_i,string(j)));
                            execstr(msprintf('xoutput.%s.l%s.i=justify(xinput.%s(j).i,''l'');',field_i,string(j),field_i));
                            execstr(msprintf('xoutput.%s.l%s.v=[];',field_i,string(j)));
                            execstr(msprintf('xoutput.%s.l%s.v=justify(xinput.%s(j).v,''l'');',field_i,string(j),field_i));
                         end
                    end


                else
                    execstr(msprintf('xoutput.%s=xinput.%s;',field_i,field_i));   // recopie identique
                end
            end


    else
        error('the input should be a div or a struture of divs')
    end

    execstr(msprintf('%s=xoutput;',varname))


    execstr(msprintf('savematfile %s %s -mat -v6',filename,varname)) // -v6 car -v7 ne marche pas!





endfunction
