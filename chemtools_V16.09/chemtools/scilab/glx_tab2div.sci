function [data] = glx_tab2div(filename)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  // reads a .csv file and converts it to .div
  // the first line and the first column of the .csv file must be labels
  // the cell in position (1,1) is ommited
  // updated: 2015, august, 06. by JMR


// partie commentée par JMR, car rien n'oblige :
// - un fichier csv a se nommer .csv
// - un nom de fichier à etre en minuscules

//  filename=convstr(filename,'u');
//  aux=strindex(filename,'.CSV');
//  if(isempty(aux))
//     filename=[filename + '.CSV'];
//  end


    m=read_csv(filename,'\t');
    data=div(0);
    data.d=strtod(strsubst(m(2:$,2:$),'D','E'));
    data.i=stripblanks(m(2:$,1));
    data.v=m(1,2:$)';


endfunction
