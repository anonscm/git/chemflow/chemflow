function listdiv2mat(xinput,varname,filename)

    // xinput: list of structure Div ou structure comprenenant comme éléments des structuress ou des fichiers classiques
    // varname: - partie du nom du fichier de sortie, qui sera: 'varname'.mat
    //          - également nom de la structure qui est sauvegardée dans le .mat
    // JCB 19/02/16

    //filename=varname +'.mat';

    if typeof(xinput)=='list' then   // xdiv est bien une structure Div
       nb=size(xinput);
       for i:1:nb
       if typeof(xinput(i))=='div' then
       exec('~/chemflow/tools/chemtools/scilab/div2mat.sci', -1)
       div2mat(res,'res_reg',"${model}")
       end
    else
        error('the input should be a liste of div or a struture of divs')
    end

    execstr(msprintf('%s=xoutput;',varname))


    execstr(msprintf('savematfile %s %s -mat -v6',filename,varname)) // -v6 car -v7 ne marche pas!





endfunction
