function [resfinal]=pop_dtune(D,maxdim,varargin)

    // nouveau:   D maxdim   (xg0) (classes_obs)      x y split func cent varargin


  if type(varargin(4))==10 then
      flag=2;   // D+ maxdim; pas de classes ech ou observation
  elseif type(varargin(5))==10 then
      flag=3    // D + maxdim + classes_perturb
      xg0=varargin(1);
      xg0=div(xg0);
  elseif type(varargin(6))==10 then
      flag=4;   // D + maxdim + classes_perturb + classes_obs
      xg0=varargin(1);
      xg0=div(xg0);
      classes_observ=varargin(2);
      classes_observ=div(classes_observ);
      classes2=classes_observ.d;
      if isdisj(classes2)=='F' then          // classes2 est disjonctif
        classes2=str2conj(classes2);
        disj_echant=conj2dis(classes2);
      end
  end




   D=div(D);
   [nxg,pxg]=size(D.d);




  //ACP non centrée (IIR: pas demandé; EPO: déjà centré avec xgi)
  model_pca=pcana(D,0);
  ndim=min(maxdim,rank(D.d));

  pop=model_pca.eigenvec.d(:,1:ndim);     //POP=vecteurs propres utilisés pour projection orthogonale

  ev_pcent=model_pca.ev_pcent;
  ev_pcent.infos(1)='number of detrimental dimensions';


  // calcul du lambda de Wilks
  if type(varargin(4))~=10 then            //10=string
     w.d=zeros(ndim+1,1);
     w.d(1)=wilks(xg0.d,disj_echant);
     for i=1:ndim;
       xi=xg0.d*(eye(pxg,pxg)-pop(:,1:i)*pop(:,1:i)');
       w.d(i+1)=wilks(xi,disj_echant);
     end
     w.v='nb of PCs';
     w.i='POP'+string([0;[1:1:size(w.d,1)-1]']);
     res_Wilks=div(w);
     res_Wilks(5)=['number of removed detrimental dimensions';'Wilks lambda'];
  end



  // calcul des RMSECVs
  lv=varargin(flag+4);
  rmsecvtot0=zeros(lv,ndim+1);
  regmodels=list();
  for j=1:(ndim+1);    // début de la boucle EPO/TOP -------------------------
      if j==1 then
      epo_corr=eye(pxg,pxg);
    else
      epo_corr=eye(pxg,pxg)-pop(:,1:j-1)*inv(pop(:,1:j-1)'*pop(:,1:j-1))*pop(:,1:j-1)';
    end
    // Application de POP à une régression
    x=varargin(flag-1);
    x=div(x);

    execstr('[reg_model]= cvreg (x.d*epo_corr, varargin(flag:$));')

    rmsecvtot0(:,j)=reg_model.err.d(:,2);
    regmodels(j)=reg_model;

  end  // fin de la boucle EPO/TOP --------------------------------------------



  // sorties Div:

  resfinal.d_matrix=[];
  resfinal.d_matrix=D;

  resfinal.eigenvec=[];
  resfinal.eigenvec=model_pca.eigenvec;
  resfinal.eigenvec.infos(2)='scores of the normalized detrimental eigenvectors';

  resfinal.ev_pcent=[];
  resfinal.ev_pcent=ev_pcent;


  if type(varargin(4))~=10 then
    resfinal.wilks=[];
    resfinal.wilks=res_Wilks;
  end

  resfinal.rmsecv.d=rmsecvtot0;
  resfinal.rmsecv.v='POP'+ string([0:1:ndim]');
  resfinal.rmsecv.i='LV'+ string([1:1:lv]');
  resfinal.rmsecv=div(resfinal.rmsecv);
  resfinal.rmsecv.infos=['dimension of the calibration model';'RMSECV after a pretreatment by orthogonal projection'];

  resfinal.pls_models=regmodels;    //div: voir plus haut

  resfinal.xcorr.d=x.d*epo_corr;
  resfinal.xcorr.i=x.i;
  resfinal.xcorr.v=x.v;

  resfinal.epo_corr.d=epo_corr;
  resfinal.epo_corr.i=x.i;
  resfinal.epo_corr.v=x.v;

endfunction
