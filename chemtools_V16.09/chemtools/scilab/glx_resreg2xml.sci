function glx_resreg2xml(res,xmlfile)

    // res contient les champs: err, ypredcv,b,scores,loadings,x_mean,y_mean,center
    doc = xmlDocument('toto.xml');
    root=xmlElement(doc,"root");
    doc.root=root;

    err      = xmlElement(doc,"err");
    ypredcv  = xmlElement(doc,"ypredcv");
    b        = xmlElement(doc,"b");
    scores   = xmlElement(doc,"scores");
    loadings = xmlElement(doc,"loadings");
    x_mean   = xmlElement(doc,"x_mean");
    y_mean   = xmlElement(doc,"y_mean");
    centrer  = xmlElement(doc,"center");
       
    doc.root.children(1)=err;
    doc.root.children(2)=ypredcv;
    doc.root.children(3)=b;
    doc.root.children(4)=scores;
    doc.root.children(5)=loadings;
    doc.root.children(6)=x_mean;
    doc.root.children(7)=y_mean;
    doc.root.children(8)=centrer;
 
    glx_div2xml(doc.root.children(1), res.err)
    glx_div2xml(doc.root.children(2), res.ypredcv)
    glx_div2xml(doc.root.children(3), res.b)
    glx_div2xml(doc.root.children(4), res.scores)
    glx_div2xml(doc.root.children(5), res.loadings)
    glx_div2xml(doc.root.children(6), res.x_mean)
    glx_div2xml(doc.root.children(7), res.y_mean)
    glx_div2xml(doc.root.children(8), res.center)

    xmlWrite(doc,xmlfile,%t)

    xmlDelete('all')

endfunction



