function[b,t,ploads,dof]=calpcr(x,y,nf)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013


  stacksize('max');

  [n,p]=size(x);

  exec(getenv('PATH_CHEM')+'/scilab/glx_pcana.sci', -1)
  res=glx_pcana(x,0,0);

  T_coord=res.scores.d(:,1:nf)
  P_eigenvec2=res.eigenvec.d

  b=zeros(p,nf);
  for i=1:nf;
     b(:,i)= P_eigenvec2(:,1:i)*inv(T_coord(:,1:i)'*T_coord(:,1:i))*T_coord(:,1:i)'*y;
  end;

  t=T_coord;
  ploads=P_eigenvec2(:,1:nf);

  dof=[1:1:nf]';

endfunction
