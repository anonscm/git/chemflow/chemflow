function[result]=cspcana(x)

  // s_pca        principal component analysis 
  
  // this function replaces the 'pca' and 'pca1' Saisir functions; 
  // however the 'pca' function (number of observations < number of variables) 
  // has been dropped because of its low numerical stability   
  
  // updated: 2012, September, 20
 
  x0=div(x);
  
  [nout,nin] = argn(0) 
  
  //if(nin==2)
  //      centered=nargin(1);
  //      if nargin(1)~=0 & nargin(1)~=1 then error('wrong value for the centering option');
  //      end
  //else  centered=0;                              //default = NOT centered 
  //end

  xc=centering(x0);  
  saisir=standardize(xc);
  
  stacksize('max');
  [n,p]=size(saisir.d);

  x_mean.d=mean(x0.d,1);  // corrige 1/06/16: saisir remplace par x0
  x_stdev.d=std(x0.d);
  
  xcentred=saisir.d; //-ones(n,1)*x_mean.d*centered;
  r=rank(xcentred);
  
  xcpxc=xcentred'*xcentred;    // meilleure SVD sur X'X que sur X 
  xcpxc=(xcpxc+xcpxc')/2;      // donc X'X est bien parfaitement symétrique    
  
  [U,S,V]=svd(xcpxc);

   eigenval.d=diag(S);
   eigenval.d=eigenval.d(1:r);
   eigenvec.d=V(:,1:r);

  for i=1:r;                // pour avoir des vecteurs propres "positifs"
      if sum(eigenvec.d(:,i))<0 then eigenvec.d(:,i)=-eigenvec.d(:,i);
      end
  end

  ind_scores.d=xcentred*eigenvec.d; 
  
  // identifiers
  eigenvec.i=saisir.v;
  eigenvec.v='PC' + string([1:1:r]');
 
  eigenval.i=eigenvec.v; 
  eigenval.v='eigenvalues';

  ev_pcent=eigenval;  // valeurs propres en pourcentage
  ev_pcent.d=100*ev_pcent.d/sum(ev_pcent.d);
  
  aux=round(eigenval.d'/sum(eigenval.d)*1000)/10;
  ind_scores.i=saisir.i;   
  ind_scores.v=eigenvec.v + '  '+ string(aux([1:1:r]))' + ' %'; 
  
  x_mean.d=x_mean.d';
  x_mean.i=saisir.v; 
  x_mean.v='x_mean';

  x_stdev.d=x_stdev.d';
  x_stdev.i=saisir.v;  
  x_stdev.v='x_stdev';
 
  var_scores.d=eigenvec.d .* (ones(p,1)*sqrt(eigenval.d)');
  var_scores.i=eigenvec.i;
  var_scores.v=eigenvec.v; 



  // sorties div:

  result.scores=[];
  result.scores=div(ind_scores);
  result.scores(5)=['observations';'scores of the observations onto the eigenvectors'];
  
  result.var_scores=[];
  result.var_scores=div(var_scores);  
  result.var_scores(5)=['initial variables';'scores of the raw eigenvectors'];  
    
  result.eigenvec=[];
  result.eigenvec=div(eigenvec);
  result.eigenvec(5)=['initial variables';'scores of the normalized eigenvectors'];

    
  result.eigenval=[];
  result.eigenval=div(eigenval);
  result.eigenval(5)=['dimension';'eigenvalues'];
  
  result.ev_pcent=[];
  result.ev_pcent=div(ev_pcent);
  result.ev_pcent(5)=['number of dimensions';'eigenvalues p.cent'];
  
  result.x_mean=[];
  result.x_mean=div(x_mean);
  
  result.x_stdev=[];
  result.x_stdev=div(x_stdev);
  
  result.centred=1;
  
  result.std=1;
  

endfunction









