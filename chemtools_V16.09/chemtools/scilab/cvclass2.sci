function [model] = cvclass2(xi,yi,split,lv,func,varargin)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  //-----------------------------------------------------------------
  // Adjustment of the input data to the Saisir format (if necessary) 
  //-----------------------------------------------------------------
 
 if  argn(2)==5 then
     scale=[]
 else
     scale=varargin(1);
 end
 
 
  x2=div(xi);
  y2=div(yi);
  [test1]=isdisj(yi);
  [test2]=isconj(yi);  

  x=x2.d;
  if  size(x,1)<2 then error ('input data must contain at least 2 observations');  end
 
  if test2=='T' then
      y2.d=conj2dis(y2.d);  
  elseif test1=='F' & test2=='F' then
      error('the classification entry is neither conjunctive nor disjunctive') 
  end
  y=y2.d;
 

  if typeof(yi)=='div' then
      y2.v=yi.v;
  else 
      y2.v='CL'+ string([1:1:size(y,2)]');
  end
  
 
  
  [mx,nx] = size(x);
  [my,nbcl] = size(y);
  if mx~=my then
     error('the number of samples in x and y do not match')
  end;
  
  func2='classtest'+func;

 
 
  // VALIDATION CROISEE ------------------------------------------------

  confm_cv=zeros(nbcl,nbcl);     //conf->conf_cv

  for k=1:10;         // pour faire tourner 10 fois la validation croisée

    // détermination des blocs
    if prod(size(split))==1 then
      flag = 0;
      split = min([split,mx]);
      nbpred = floor(mx /split);
      nbpred=max(nbpred,1);                   // pour ne pas avoir 0...
      [k,mel] = gsort(rand(mx,1),'g','i');    //ordre croissant
      x = x(mel,:);
      y = y(mel,:);
    else 
      flag = 1;
      lot = split;
      clear("split");
      split = max(lot);
      mel = 1:mx;
    end;

    st = msprintf('[classif_cv,cf]=%s(calx,caly,testx,testy,lv,scale);',func2); 
  
    for i = 0:split-1;
      if flag==0 then
        deb = i*nbpred+1;
        fin = deb+nbpred-1;
        calx  =  [x(1:deb-1,:);x(fin+1:mx,:)];
        testx =  x(deb:fin,:);
        caly  =  [y(1:deb-1,:);y(fin+1:mx,:)];
        testy =  y(deb:fin,:);
      elseif flag==1 then 
        testx = x(find(bool2s(lot==i+1)),:);  
        calx  = x(find(bool2s(lot~=i+1)),:);
        testy = y(find(bool2s(lot==i+1)),:);
        caly  = y(find(bool2s(lot~=i+1)),:);
      end;
      
      execstr(st);     
      
      confm_cv=confm_cv+cf;  
    end
  end
  
  [confm_cv_pcent,err_cv,errbycl_cv]=conf_obs2pcent(confm_cv);
  


  // ETALONNAGE --------------------------------------------------
 
  execstr(msprintf('[classif_cal,confm_cal_nobs,nn2,scale]=%s(x,y,x,y,lv,varargin(:));',func2));

  [confm_cal,err_cal,errbycl_cal,nonclasse,nonclasse_bycl]=conf_obs2pcent(confm_cal_nobs,sum(y,'r'));





   // SORTIES Div:  ------------------------------------------------

  model.conf_cal_nobs.d=confm_cal_nobs;
  model.conf_cal_nobs.i='pred_'+ y2.v;
  model.conf_cal_nobs.v='ref_' + y2.v;
  model.conf_cal_nobs=div(model.conf_cal_nobs);

  model.conf_cal.d=confm_cal;
  model.conf_cal.i='pred_'+ y2.v;
  model.conf_cal.v='ref_' + y2.v;
  model.conf_cal=div(model.conf_cal);

  model.conf_cv.d=confm_cv_pcent;;
  model.conf_cv.i='pred_'+ y2.v;  
  model.conf_cv.v='ref_' + y2.v;
  model.conf_cv=div(model.conf_cv);

  model.err_cal.d=[err_cal err_cv];
  model.err_cal.v=['classification error, p.cent of the classified observations';
 'cross-validation error, p.cent of the classified observations'];
  model.err_cal=div(model.err_cal);

  model.errbycl_cal.d=errbycl_cal;
  model.errbycl_cal.i=y2.v; 
  model.errbycl_cal=div(model.errbycl_cal);
  
  model.errbycl_cv.d=errbycl_cv;
  model.errbycl_cv.i=y2.v;  
  model.errbycl_cv=div(model.errbycl_cv);
  
  model.notclassed.d=nonclasse;
  model.notclassed.v='not classed, p.cent of all the observations';
  model.notclassed=div(model.notclassed);
  
  model.notclassed_bycl.d=nonclasse_bycl;
  model.notclassed_bycl.v='not classed, p.cent of the observations of each class';
  model.notclassed_bycl=div(model.notclassed_bycl);

  model.method=func;

  model.xcal=[];
  model.xcal=x2;   //déjà Div
  
  model.ycal=[];
  model.ycal=y2;   // déjà Div
  
  model.knn=nn2;

  model.scale=scale;
  
  
  

endfunction
