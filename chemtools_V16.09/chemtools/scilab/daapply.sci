function [res_appli]=daapply(model,xv,varargin);
    
  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
   
  // varargin(1): yv si connu  
  
  
  
  
  // CAS DES METHODES DE REGRESSION -------------------------
  
  // classtestxx calcule le modèle ce qui peut être long; on a déjà
  // ces infos (loadings, metric) donc on utilise plutôt memberpred  
  
  if ~isfield(model,'knn') then
 
    xv1=div(xv);
    xval=xv1.d;
    [n,q]=size(xval);
    table1=model.conf_cv(1).d;
    nbcl=size(table1,1);
    lv=size(model.err.d,1);
    xcal=model.xcal.d;
    ycal=model.ycal.d;
    [nxc,qxc]=size(xcal);
    [nyc,qyc]=size(ycal);
    yval0=zeros(n,qyc);
  
    if q~=qxc then 
       error('not the same numbers of variables')
    end

    
    // extraction des infos du modèle
    metric=model.classif_metric;
    scale=model.scale;
    if isfield(model,'classif_opt') then
       class=model.classif_opt;
    else
       class=0;
    end
    seuil=model.threshold;
    func=model.method;
  
    xpr=model.rloadings;
 
 
    // gestion de scaling: centrage / centrage-standardisation
    vm = mean(xcal,'r');
    vst = stdev(xcal,'r');
 
    for i=1:size(xcal,2);          //gestion des variances nulles... 
      if vst(i)<10*%eps then vst(i)=0.0000000001;
      end
    end
 
    if scale=='cs' then
      xcal =  ( xcal - ones(size(xcal,1),1)  * vm ) ./ ( ones(size(xcal,1),1) * vst );
      xval = ( xval - ones(n,1) * vm ) ./ ( ones(n,1) * vst );
     
    elseif scale=='c' then
      xcal =  xcal  - ones(size(xcal,1),1) * vm;
      xval = xval - ones(n,1) * vm;
    end;


    // ---- ce code se retrouve dans classtestxx ------------------------------ 
 
    // prédiction pour le jeu de test
    yp=zeros(n,nbcl,lv); 
    
        
    // calcul des scores:    
    xcal_scores=xcal*xpr.d;
    xval_scores=xval*xpr.d;
          
    for i=1:lv;

      [pr]=memberpred(xcal_scores(:,1:i),ycal,xval_scores(:,1:i),yval0,metric,class);        

  
    // décision d'appartenance à une classe: PROBA. MAX avec la classe
      [prm,ind] = max(pr,'c');
      ypred = conj2dis(ind,nbcl,'C');
      ypred(prm<seuil,:) = 0;     // possible que des obs. ne soient rattachées à aucune classe! 
      
      yp(:,:,i)=ypred;
    end


    // ------ fin du code commun à classtestxx (ici: pas de calcul de matrice de confusion)-------


    // cas où yv est connue:
    if argn(2)==3 then
      yv=varargin(1);
      yv1=div(yv);
      yval=yv1.d;
      if isconj(yval)=='T' then
        yval=conj2dis(yval);
      elseif isdisj(yval)=='T' then
        yval=yval;
      else 
        yval=str2conj(yval);
        yval=conj2dis(yval);
      end
      
 
      confus=zeros(nbcl,nbcl,lv);
      confus_pcent=confus; 
      err_test=zeros(lv,1);
      err_test_bycl=zeros(lv,nbcl);      
      nonclasse=zeros(lv,1);
      nonclasse_bycl=zeros(lv,nbcl);
      
      for i=1:lv;
        confus_i=yp(:,:,i)'*yval; 
        confus(:,:,i)=confus_i;            
        [confusi_pcent,erri,erribycl,nci,nci_bycl]=conf_obs2pcent(confus_i,sum(yval,'r'));
        confus_pcent(:,:,i)=confusi_pcent;
        err_test(i)=erri;
        err_test_bycl(i,:)=erribycl';
        nonclasse(i)=nci;        
        nonclasse_bycl(i,:)=nci_bycl';
      end
    end

    
    
    // sorties
    
    res_appli.ypred=list();
    for i=1:lv;
      ypredi.d=yp(:,:,i);
      ypredi.i=xv1.i;
      ypredi.v='CL'+string([1:1:nbcl])';
      ypredi=div(ypredi);
      res_appli.ypred(i)=ypredi;
    end

    if  argn(2)>=3 then
      res_appli.conf_test_nobs=list();
      for i=1:lv;
        confus_i.d=confus(:,:,i); 
        confus_i.i='pred_'+ string([1:1:size(yval,2)]');
        confus_i.v='ref_'+ string([1:1:size(yval,2)]');
        confus_i=div(confus_i);
        res_appli.conf_test_nobs(i)=confus_i;
      end      
      
      res_appli.conf_test=list();
      for i=1:lv;
        conftesti.d=confus_pcent(:,:,i);
        conftesti.i='pred_'+ string([1:1:size(yval,2)]');
        conftesti.v='ref_'+ string([1:1:size(yval,2)]');
        conftesti=div(conftesti);
        res_appli.conf_test(i)=conftesti;
      end  
      
      res_appli.err_test.d=err_test;
      res_appli.err_test.i='DV'+string([1:1:lv]');
      res_appli.err_test.v='error of prediction, p.cent';
     
      
      res_appli.err_test=div(res_appli.err_test);
  
      res_appli.errbycl_test.d=err_test_bycl;
      res_appli.errbycl_test.i='DV'+string([1:1:lv]'); 
      res_appli.errbycl_test.v='CL'+string([1:1:nbcl])';
      res_appli.errbycl_test=div(res_appli.errbycl_test); 
 
      res_appli.notclassed.d=nonclasse;
      res_appli.notclassed.i='DV'+string([1:1:lv])';
      res_appli.notclassed.v='not classed, p.cent of all the observations';
      res_appli.notclassed=div(res_appli.notclassed);
    
      res_appli.notclassed_bycl.d=nonclasse_bycl;
      res_appli.notclassed_bycl.i='DV'+string([1:1:lv])';
      res_appli.notclassed_bycl.v='CL'+string([1:1:nbcl])';
      res_appli.notclassed_bycl=div(res_appli.notclassed_bycl);

      res_appli.xcal_scores=xcal_scores;
      res_appli.xval_scores=xval_scores;
      
    end
  

  
  // CAS DES AUTRES METHODES: knn pour l'instant  ----------------------------------
  elseif isfield(model,'knn') then 
    
    
    res = applyknnda(model,xv,varargin(:));
    

    // sorties  
    res_appli.ypred=[];
    res_appli.ypred=res.yclass; 

    if argn(2)==3 then
      res_appli.conf_test_nobs=[]; 
      res_appli.conf_test_nobs=res.conf; 
      res_appli.err_pcent=res.err_pcent;
 
//      res_appli.conf_test.d=confus_pcent; 
//      res_appli.conf_test.i='pred_'+ yv1.v;
//      res_appli.conf_test.v='ref_'+ yv1.v;
//      res_appli.conf_test=div(res_appli.conf_test);
//      
//      res_appli.err_test.d=err_test;
//      res_appli.err_test.v='error of prediction, p.cent';
//      res_appli.err_test=div(res_appli.err_test);
// 
//      res_appli.errbycl_test.d=err_test_bycl;
//      res_appli.errbycl_test.i='CL'+string([1:1:nbcl]');
//      res_appli.errbycl_test=div(res_appli.errbycl_test);   
//    
//      res_appli.notclassed.d=nci;
//      res_appli.notclassed.v='not classed, p.cent of all the observations';
//      res_appli.notclassed=div(res_appli.notclassed);
//    
//      res_appli.notclassed_bycl.d=nci_bycl;
//      res_appli.notclassed_bycl.i='CL'+string([1:1:nbcl])';
//      res_appli.notclassed.v='not classed, p.cent of the number of observations of each class';
//      res_appli.notclassed_bycl=div(res_appli.notclassed_bycl);
 
     end

  end
    

     
endfunction
