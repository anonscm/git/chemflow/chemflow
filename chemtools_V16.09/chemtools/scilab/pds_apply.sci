
function x_slave=pds_apply(model_pds,x_master)

    if ~isfield(model_pds,'b') | ~isfield(model_pds,'offset') then
        error('the model is not a pds model')
    end

    x=div(x_master);
    x_slave=x;

    n=size(x.d,1);
    x_slave.d=x.d*model_pds.b.d + ones(n,1)*model_pds.offset.d';



endfunction
