function glx_xstruct2xml(x0,xmlfile)

    // x est une structure avec les champs: plan1, plan2,...plann
    doc = xmlDocument(xmlfile);
    root=xmlElement(doc,"root");
    doc.root=root;
   
    x=x0;
    // Initialisation
    n=length(x)-2;      // -2 car la structure semble avoir 2 champs cachés
    

    // construction de la structure
    for i=1:n;
        name='x.'+'plan'+string(i);
        name2='plan'+string(i);

        execstr(msprintf('plani=%s',name));
  
        [ni,qi]=size(plani);
        
        // mise au format csv: separation par ','
        // note: doit pouvoir bien s'améliorer!
        xd=[];
        for j=1:ni;
            xdj=[];
            for k=1:qi-1;
                xdj=xdj + string(plani(j,k)) +',';
            end
            xdj=xdj+string(plani(j,qi)); 
            xd=[xd;xdj];
        end
        
       
        execstr(msprintf("%s=xmlElement(doc,name2)",name2))
       
        execstr(msprintf('doc.root.children(i)=%s;',name2));
   
        doc.root.children(i).content=string(xd);
       
    end
    
    xmlWrite(doc,xmlfile,%t)

    xmlDelete('all')

endfunction



