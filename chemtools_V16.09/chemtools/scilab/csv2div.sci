function [data] = csv2div(filename)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  // reads a .csv file (separation = ;) and converts it to .div
  // the first line and the first column of the .csv file must be labels
  // the cell in position (1,1) is ommited
  // updated: 2014, october, 24.

  //filename=convstr(filename,'u');
  aux=strindex(filename,'.csv');
  if(isempty(aux))
     filename=[filename + '.csv'];
  end

  a=mgetl(filename);
  a=strsubst(a,',','.');     // si délimiteur décimal ',' il est remplacé par '.'
  a=strsubst(a,'""','');

  for i=2:size(a,1);
     xline=a(i);
     aux1=strindex(xline,';');
     separation=strsplit(xline,aux1(1)-1);
        // division of file into ident name and data

     sep2=separation(2);     // separation(2) = data separated by ;
     sep2=strsplit(sep2,';');
     sep2=strtod(sep2)';     // conversion to double
     sep2=sep2(2:$);

     data.d(i-1,:)=evstr(sep2);
     data.i(i-1)=stripblanks(separation(1));
  end

  xline=a(1);
  aux1=strindex(xline,';');
  vector=strsplit(xline,aux1);     // division of file into ident name and data
  vector=strsubst(vector,';','');  // remove ;
  vector=stripblanks(vector);      // remove blanks
  data.v=vector(2:size(vector,1));

  //div
  data=div(data);


endfunction
