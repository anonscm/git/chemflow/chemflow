function [conf_matrix,lv_max,metric,scale,class,seuil] = classtestxx(calx,caly,testx,testy,lv,func,varargin)


  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  // Number of arguments in function call
  [nargout,nargin] = argn(0)

  testdisj=isdisj(caly);

  nbcl = size(caly,2);

  // gestion des arguments d'entrée par défaut (copie dans cvclass)
  if nargin==6 then
     metric=0;
     scale = 'cs';
     class = 0;
     seuil = 1/nbcl;
  elseif nargin==7 then
     metric=varargin(1);
     scale='cs';
     class = 0;
     seuil = 1/nbcl;
  elseif nargin==8 then
     metric=varargin(1);
     scale=varargin(2);
     class=0;
     seuil=1/nbcl;
  elseif nargin==9 then
     metric=varargin(1);
     scale=varargin(2);
     class=varargin(3);
     seuil=1/nbcl;
  elseif nargin==10 then
     metric=varargin(1);
     scale=varargin(2);
     class=varargin(3);
     seuil=varargin(4);    
  else error('wrong number of input arguments in classtestxx') 
  end;
 
  if  seuil=='m' then
    seuil=1/nbcl;
  end
 
  // validation de l'option scale
  if  scale~='c' & scale~='cs' then
    error ('scale options are c centered or cs centered+standardized only')
  end
 
  n=size(testx,1);
  [n_calx,q]=size(calx);   // nbre de variables
  
  lv_max=min(lv,q);
  
  // gestion de scaling: centrage / centrage-standardisation
  vm = mean(calx,'r');
  vst = stdev(calx,'r');
 
  for i=1:size(calx,2);          //gestion des variances nulles... 
      if vst(i)<10*%eps then vst(i)=0.0000000001;
      end
  end
 
  if scale=='cs' then
     calx =  ( calx - ones(size(calx,1),1)  * vm ) ./ ( ones(size(calx,1),1) * vst );
     testx = ( testx - ones(n,1) * vm ) ./ ( ones(n,1) * vst );
     
  elseif scale=='c' then
     calx =  calx  - ones(size(calx,1),1) * vm;
     testx = testx - ones(n,1) * vm;
  end;


  // calcul du modèle sur l'ensemble d'étalonnage
  functionname='class'+func;
  execstr(msprintf('[resclass]= %s (calx,caly,lv_max);',functionname)); 
 


 
  // xpr vérifie:  t = x * xpr pour MLR(xpr=I), PCR(xpr=P), SIMPLS (xpr=R) 
  if isfield(resclass,'loadings') then
    xpr=resclass.loadings; 
  elseif isfield(resclass,'rloadings') then
    xpr=resclass.rloadings;
  elseif ~isfield(resclass,'loadings') & ~isfield(resclass,'rloadings') then
    p_calx=size(calx,2);      // nbre de variables dans calx
    xpr=eye(p_calx,p_calx);
  end


  
  //définition des sorties
  ep = zeros(lv_max,1); 
  cf = hypermat([nbcl nbcl lv_max]);
  ep_class=zeros(nbcl,lv_max);

 
  // -----  ce code se retrouve dans daapply ----------------------------------
 
  // test, pour 1 a lv vecteurs discriminants 
  // ON CALCULE LES SCORES obtenus avec les méthodes fda, plsda, pls2da ou forwda
  cf=zeros(nbcl,nbcl,lv_max);
 
  if func=='fda' | func=='forwda' | func=='plsda' | func=='copda' then 
      calx_scores=zeros(n_calx,lv_max,nbcl); 
      testx_scores=zeros(n,lv_max,nbcl);
  end
  
 
  for k = 1:lv_max;  
 
    calx_scores=calx*xpr(:,1:k);
    testx_scores=testx*xpr(:,1:k);
                        
    [pr] = memberpred(calx_scores,caly, testx_scores,testy,metric,class);

     

    // décision d'appartenance à une classe: PROBA. MAX avec la classe
    [prm,ind] = max(pr,'c');
    pred = conj2dis(ind,nbcl,'C');  //'C' force à imposer conjonctif
    pred(prm<seuil,:) = 0;     // possible que des obs. ne soient rattachées à aucune classe! 
    
    // calcul de la matrice de confusion
    c = pred'*testy;
    cf(:,:,k)=c;
    
  end;
 
  // ----------- fin du code commun avec daapply -------------------------------
  
  conf_matrix=list();
  
  for i=1:lv_max;
      cf_i.d=cf(:,:,i);
      cf_i.i='pred_'+string([1:1:nbcl]');
      cf_i.v='ref_'+string([1:1:nbcl]');
      conf_matrix(i)=[];
      conf_matrix(i)=div(cf_i);
  end

endfunction











