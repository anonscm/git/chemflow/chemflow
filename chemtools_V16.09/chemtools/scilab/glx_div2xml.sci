function glx_div2xml(doc_element, x)

    // sous-fonction utilisee dans glx_resreg2xml.sci
    x=div(x);
    [n,q]=size(x.d);

    // transformation des donnees numeriques en chaines de caracteres separees par ','
    xd=[];
    for i=1:n;
        xdi=[];
        for j=1:q-1;
            xdi=xdi + string(x.d(i,j)) +',';
        end
        xdi=xdi+string(x.d(i,q)); 
        xd=[xd;xdi];
    end

    xmlAppend(doc_element,xmlElement(doc,"d"));
    xmlAppend(doc_element,xmlElement(doc,"i"));
    xmlAppend(doc_element,xmlElement(doc,"v"));

    doc_element.children(1).content = xd;
    doc_element.children(2).content = x.i;
    doc_element.children(3).content = x.v;


endfunction

