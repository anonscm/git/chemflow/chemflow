function [resfinal]=pop_dtop_optim(x,y,xsm,xss,maxdim,lv)


xg=div(x);
[nxg,pxg]=size(xg.d);


[nxsm,psm]=size(xsm.d);

// initialisation de D
D.d=[];
D.i=[];
D.v=[];

// calcul de D
D.d=xsm.d-xss.d;
D.i='diff of standard' + string([1:1:nxsm]');
D.v=xsm.v;

 D=div(D);
 [nxg,pxg]=size(D.d);

  //ACP non centrée (IIR: pas demandé; EPO: déjà centré avec xgi)
  model_pca=pcana(D,0,0);
  ndim=min(maxdim,rank(D.d));

  pop=model_pca.eigenvec.d(:,1:ndim);     //POP=vecteurs propres utilisés pour projection orthogonale

  ev_pcent=model_pca.ev_pcent;
  ev_pcent.infos(1)='number of detrimental dimensions';

  xg0=[xsm;xss];
  disj_echant=[repmat(1,nxsm,1);repmat(2,nxsm,1)];


  rmsecvtot0=zeros(lv,ndim+1);

  // calcul du lambda de Wilks
     w.d=zeros(ndim+1,1);
     // w.d(1)=wilks(xg0.d,disj_echant);
     for i=1:ndim+1;
       method='pls';
       if i==1 then
       w.d(i)=wilks(xg0.d,disj_echant);
       proj_ortho=eye(pxg,pxg);
       calcorr=x;
       calcorr.d=calcorr.d*proj_ortho;
       execstr('[reg_model]= cvreg (calcorr,y,4,method,1,lv);')
       rmsecvtot0(:,i)=reg_model.err.d(:,2);
       else
       proj_ortho=eye(pxg,pxg)-pop(:,1:i-1)*pop(:,1:i-1)';
       stcorr=xg0;
       stcorr.d=xg0.d*proj_ortho;
       w.d(i)=wilks(stcorr,disj_echant);
       calcorr=x;
       calcorr.d=calcorr.d*proj_ortho;
       execstr('[reg_model]= cvreg (calcorr,y,4,method,1,lv);')
       rmsecvtot0(:,i)=reg_model.err.d(:,2);
       end
     end
     w.v='nb of PCs';
     w.i='POP'+string([0;[1:1:size(w.d,1)-1]']);
     res_Wilks=div(w);
     res_Wilks(5)=['number of removed detrimental dimensions';'Wilks lambda'];

  // sorties Div:

  resfinal.d_matrix=[];
  resfinal.d_matrix=D;

  resfinal.eigenvec=[];
  resfinal.eigenvec=model_pca.eigenvec;
  resfinal.eigenvec.infos(2)='scores of the normalized detrimental eigenvectors';

  resfinal.ev_pcent=[];
  resfinal.ev_pcent=ev_pcent;


  resfinal.wilks=[];
  resfinal.wilks=res_Wilks;

  resfinal.rmsecv.d=rmsecvtot0;
  resfinal.rmsecv.v='POP'+ string([0:1:ndim]');
  resfinal.rmsecv.i='LV'+ string([1:1:lv]');
  resfinal.rmsecv=div(resfinal.rmsecv);
  resfinal.rmsecv.infos=['dimension of the calibration model';'RMSECV after a pretreatment by orthogonal projection'];



endfunction
