function res=xml2resreg(xmlfile)

    // lecture d'un fichier .xml contenant le resultat d'une regression et ecriture sous forme d'une structure

    doc2=xmlRead(xmlfile)
    n_doc2=size(doc2.root.children);
    name_doc2=xmlName(doc2.root.children);
    res=[];
    for j=1:n_doc2(1,2);
        data0=doc2.root.children(j).children(1).content;
        xdata.d=[];
        xdata.i=doc2.root.children(j).children(2).content;
        xdata.v=doc2.root.children(j).children(3).content;

        n=size(xdata.i,1);
        q=size(xdata.v,1);
        for i=1:n;
            line= strsplit(data0(i),',',q);
            line=strtod(line);
            xdata.d=[xdata.d;line'];
        end

        xdata=div(xdata)

        execstr(strcat(['res.',name_doc2(j),'=[];']));
        execstr(strcat(['res.',name_doc2(j),'=xdata;']));
        if name_doc2(j)=="center" then res.center=xdata.d;end
    end








endfunction
