function struct2xml(x,xmlfile)

    // x est une structure
    doc = xmlDocument(xmlfile);
    root=xmlElement(doc,"root");
    doc.root=root;


    // Initialisation
    n=length(x);


    // construction de la structure
    for i=1:n;
        name=getfield(1,x)
        name=name(i)+string(i)
        if typeof(x)=='list'
        then struct2xml(x,xmlfile)
        elseif typeof(x)=='st'
        then exec(getenv('PATH_CHEM')+'/scilab/resreg2xml.sci', -1)
        kill = resreg2xml(x,xmlfile)
        else
        plani=x(i);
        [ni,qi]=size(plani);

        // mise au format csv: separation par ','
        // note: doit pouvoir bien s'améliorer!
        xd=[];
        for j=1:ni;
            xdj=[];
            for k=1:qi-1;
                xdj=xdj + string(plani(j,k)) +',';
            end
            xdj=xdj+string(plani(j,qi));
            xd=[xd;xdj];
        end


        execstr(msprintf("%s=xmlElement(doc,name)",name))

        execstr(msprintf('doc.root.children(i)=%s;',name));

        doc.root.children(i).content=string(xd);

    end

    xmlWrite(doc,xmlfile,%t)

    xmlDelete('all')

endfunction
