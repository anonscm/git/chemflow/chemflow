function [yh] = classknnda(calx,caly,testx,nn,scal)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013
 
  //initialisation
  if isconj(caly)=='T'  then  caly=conj2dis(caly);    end
  //if isconj(testy)=='T' then  testy=conj2dis(testy);  end
  nbcl = size(caly,2);

  //if argn(2) < 6 then   scal = [];   end;    

  [n,p] = size(calx);
  [m,q] = size(testx);
  [o,r] = size(caly);


  // calcul des predictions
  yh = zeros(m,r);
  for i=1:m    //m=nbre d'observations dans testx
    z = ones(n,1)*testx(i,:)-calx;
    if isdef('scal') & scal == 's'  then
        s = std(calx);
        z = z ./ (ones(n,1)*s);
    end;
    d = diag(z*z')';
    [ds,ind]=gsort(d,'c','i');  
    vois = ind(1:nn);
    score = sum(caly(vois,:),1);
    [sm,j] = max(score);
    yh(i,j) = 1;
  end;
  
 
endfunction
