function lot = cvgroups(mx,split)
 
  // mx:        nombre de valeurs pour lesquelles un regroupement est nécessaire
  // split:     manière dont le regroupement est obtenu
  //            ex: 'jck5'  5 groupes par Jack Knife
  //                 'blk5' 5 gtoupes contigus
  //                 'vnb5' 5 groupes par Stores Vénitiens
  //                  5     5 groupes tirés au hasard   
  // lot:       vecteur de longueur mx codant les split groupes (1,2,...split)
  //            pour chaque observation

  flag=0;   // correction de bug, 5 avril 16: pour avoir tous les groupes représentés 

  while flag==0    

  if type(split)==10  then  // if split is a string


     if     part(split,1:3)=="vnb"  then  // venitian blind
            nsplit = strtod(part(split,4:length(split)));
            bysplit = round(mx/nsplit);

            lot=[];
            for i=1:bysplit+1;
              lot=[lot; [1:1:nsplit]']; 
            end

            lot = lot(1:mx);

     elseif part(split,1:3)=="blk" | part(split,1:3)=="jck"  then  // blocks =  jacknife      
            nsplit = strtod(part(split,4:length(split)));
            bysplit = round(mx/nsplit);
            lot = ones(mx,1).*nsplit;
            for i=1:nsplit
              lot((i-1)*bysplit+1:i*bysplit)=i;
            end
            lot = lot(1:mx);

     else
                error('Incorrect split argument') 
     end;

  else // if split is not a string

        if prod(size(split))==1  then  // simple random subsets 
 
            if split >= mx      // leave one out
                lot = (1:mx)';
            else                // random subset
                lot=ceil(rand(mx,1)*split);
            end;           
            
        else                      // simple fixed subsets
            if prod(size(split))==2  then  // double random subsets 
                lot=ceil(rand(mx,split(2))*split(1));
            else
                if size(split,1)==mx  then // double fixed subsets
                    for k=1:size(split,2)
                        lot(:,k)=str2conj(split(:,k));
                    end;
                else
                    error('Incorrect split argument') 
                end;
                flag=1;

            end;
        end;
    end
    
    
    
    // vérification que tous les groupes sont représentés (les colonnes de lot = conjonctives)

    if (type(split)==10 | (type(split)~=10 & prod(size(split))==1)) & isconj(lot)=='T' then
        flag=1;
    elseif type(split)~=10 & prod(size(split))==2 then
        [n,p]=size(lot);
        ktot=0;
        for i=1:p;
           if isconj(lot(:,i))=='T' then  // on compte les "bonnes" colonnes
               ktot=ktot+1;               // modifs 14juin16
           end             
        end
        if ktot==p then
            flag=1;
        end
    end     


    end   // end du flag 
   
    
endfunction
