function [model] = cvclass(xi,yi,split,lv,func,varargin)

  // distributed under the CeCILL-C license
  // copyright INRA-IRSTEA 2013

  //-----------------------------------------------------------------
  // Adjustment of the input data to the Saisir format (if necessary)
  //-----------------------------------------------------------------
  x2=div(xi);
  if typeof(yi)=='div' then
    y2.d=conj2dis(yi.d);
    y2=div(y2.d);
  elseif typeof(yi)=='st' then
    y2.d=conj2dis(yi.d);
    y2=div(yi.d);
  elseif typeof(yi)=='string' then
    [y2.d,y2.v]=str2conj(yi);
    y2.d=conj2dis(y2.d);
    y2=div(y2);
  elseif typeof(yi)=='constant' & min(size(yi))==1 then
    yi=conj2dis(yi);
    y2=div(yi);
  elseif typeof(yi)=='constant' & min(size(yi))>1 then
    y2=div(yi);
  end

  x=x2.d;


  if  size(x,1)<2 then error ('input data must contain at least 2 observations');  end

  // gestion de y2.d
//  if test2=='T' then
//      y2.d=conj2dis(y2.d);
//  elseif test1=='T' then
//      y2.d=y2.d;
//  else
//      y2.d=str2conj(y2.d);
//      y2.d=conj2dis(y2.d);
//  end

  y=y2.d;

  // gestion de y2.v:
  if size(y2.v)~=size(y2.d,2) then
    y2.v='cl_'+string([1:1:size(y2.d,2)]');
  end

  if size(y2.v)==1 then
      error('y contains only one class')
  end

  //pause


  [mx,nx] = size(x);
  [my,nbcl] = size(y);
  if mx~=my then
     error('the number of samples in x and y do not match')
  end;


 // ETALONNAGE

  [confus_m0,lv_max,metric,scale,class,seuil]=classtestxx(x,y,x,y,lv,func,varargin(:));

  lv2=min(lv,lv_max);

  confus_m=list();
  for i=1:lv2;
      confus_m(i)=confus_m0(i);
  end

  //confus_m=confus_m(1:lv2);

  //pause

  // calcul des pourcentages de non classés

  nonclasse=zeros(lv2,1);
  nonclasse_bycl=zeros(lv2,nbcl);

  for i=1:lv2;
    conf_i=confus_m(i).d;
    [nul1,nul2,nul3,nci,nci_bycl]=conf_obs2pcent(conf_i,sum(y,'r'));
    nonclasse(i)=nci;
    nonclasse_bycl(i,:)=nci_bycl';
  end

  // matrice de confusion d'étalonnage mise en pourcentage
  ec=zeros(lv2,1);
  eclasses=zeros(nbcl,lv2);
  confus_pcent=confus_m;
  for i=1:lv2;
    [c_i,ec_i,eclasses_i]=conf_obs2pcent(confus_m(i).d);
    confus_pcent(i)=c_i;
    ec(i)=ec_i;
    eclasses(:,i)=eclasses_i

  end



  // CALCUL DU MODELE (fda, plsda, forwda)
  func2='class'+func;
  execstr(msprintf('[resclass]= %s (x,y,lv);',func2));


  // VALIDATION CROISEE -----------------------------------------------

  //st = 'cf=classtestxx(calx,caly,testx,testy,lv_max,func,varargin(:));';

  conf_cv=list();
  for i=1:lv2;
     conf_cv(i)=zeros(nbcl,nbcl);
  end

  //conf_cv=zeros(nbcl,nbcl,lv2);

  for k=1:10;   // pour faire tourner 10x la validation croisée avec les blocs
                // indispensable car si tous les ech. d'une classe sont dans un seul jeu (test ou cal)
                // ils ne peuvent pas être prédits
                // avec 10 répèts: on a + de chances d'une dispersion de tous les échantillons d'une même classe
                // pour partie dans calx et pour partie dans testx

    // détermination des blocs
    if prod(size(split))==1 then
      flag = 0;
      split = min([split,mx]);
      nbpred = floor(mx /split);
      nbpred=max(nbpred,1);                   // pour ne pas avoir 0...
      [k,mel] = gsort(rand(mx,1),'g','i');    //ordre croissant
      x = x(mel,:);
      y = y(mel,:);
    else
      flag = 1;
      lot = split;
      clear("split");
      split = max(lot);
      mel = 1:mx;
    end;

    // test de chaque paquet
    for i = 0:split-1;
      if flag==0 then
        deb = i*nbpred+1;
        fin = deb+nbpred-1;
        calx  =  [x(1:deb-1,:);x(fin+1:mx,:)];
        testx =  x(deb:fin,:);
        caly  =  [y(1:deb-1,:);y(fin+1:mx,:)];
        //pause

        testy =  y(deb:fin,:);
      elseif flag==1 then
        testx = x(find(bool2s(lot==i+1)),:);
        calx  = x(find(bool2s(lot~=i+1)),:);
        testy = y(find(bool2s(lot==i+1)),:);
        caly  = y(find(bool2s(lot~=i+1)),:);
      end;

      //execstr(st);     //voir ligne 111
      cf=classtestxx(calx,caly,testx,testy,lv_max,func,varargin(:));

      for j=1:lv2;
        conf_cv(j)=conf_cv(j)+cf(j).d;
      end;
    end;

  end

  // matrice de confusion en pourcentage; erreurs de CV globales et par classe
  ep=zeros(lv2,1);
  ep_classes=zeros(nbcl,lv2);

  for i=1:lv2;
    [confcv_i,ep_i,ep_classes_i]=conf_obs2pcent(conf_cv(i));
    conf_cv(i)=confcv_i;
    ep(i)=ep_i;
    ep_classes(:,i)=ep_classes_i
  end;





  // SORTIES
  // mise des hypermaytrices de confusion sous forme de listes Div

  confus_cal_nobs=list();
  confus_cal=list();
  confus_cv=list();

  for i=1:lv2;
    tempo_1.d=confus_m(i).d;
    tempo_1.i='pred_'+ y2.v;
    tempo_1.v='ref_'+ y2.v;


    confus_cal_nobs(i)=div(tempo_1);
    tempo_2.d=confus_pcent(i);
    tempo_2.i=tempo_1.i;
    tempo_2.v=tempo_1.v;



    confus_cal(i)=div(tempo_2);
    tempo_3.d=conf_cv(i);
    tempo_3.i=tempo_1.i;
    tempo_3.v=tempo_1.v;
    confus_cv(i)=div(tempo_3);
  end


  model.conf_cal_nobs=[];
  model.conf_cal_nobs=confus_cal_nobs;

  model.conf_cal=[];
  model.conf_cal=confus_cal;

  model.conf_cv=[];
  model.conf_cv=confus_cv;

  model.err.d=[ec ep];
  model.err.i='DV'+string([1:1:lv2]');
  model.err.v=['error of classification, p.cent'; 'error of cross-validation, p.cent'];
  model.err=div(model.err);
  model.err(5)=['number of dimensions';'p.cent errors of classification and/or CV'];

  model.errbycl_cal.d=eclasses';
  model.errbycl_cal.i='DV'+string([1:1:lv2]');
  model.errbycl_cal.v=y2.v;
  model.errbycl_cal=div(model.errbycl_cal);
  model.errbycl_cal.infos=['number of discriminant variables';'classification error for each class'];

  model.errbycl_cv.d=ep_classes';
  model.errbycl_cv.i='DV'+string([1:1:lv2]');
  model.errbycl_cv=div(model.errbycl_cv);
  model.errbycl_cv.infos=['number of discriminant variables';'CV error for each class'];

  model.notclassed.d=nonclasse;
  model.notclassed.i='DV'+string([1:1:lv2]');
  model.notclassed.v=['not classed, p.cent of all the observations'];
  model.notclassed=div(model.notclassed);
  model.notclassed(5)=['number of discriminant variables';'p.cent of not classified observations'];

  model.notclassed_bycl.d=nonclasse_bycl;
  model.notclassed_bycl.i='DV'+string([1:1:lv2]');
  model.notclassed_bycl.v=y2.v;
  model.notclassed_bycl=div(model.notclassed_bycl);
  model.notclassed_bycl(5)=['number of discriminant variables';'p.cent of not classified observations, for each class'];

  model.method=func;

  model.xcal=[];
  model.xcal=x2;

  model.ycal=[];
  model.ycal=y2;


  model.loadings.d=resclass.loadings;
  model.loadings.i=x2.v;
  model.loadings.v='DV'+string([1:1:lv2]');
  model.loadings=div(model.loadings);
  model.loadings(5)=['initial variables';'scores of discriminant variables'];

  if isfield(resclass,'m_metric') then
    model.model_metric.d=resclass.m_metric;
    model.model_metric.i=x2.v;
    model.model_metric.v=x2.v;
    model.model_metric=div(model.model_metric);
  end

  model.classif_metric=metric;

  model.scale=scale;

  model.classif_opt=class;

  model.threshold=seuil;



endfunction
