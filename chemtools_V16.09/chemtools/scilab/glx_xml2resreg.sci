function res=glx_xml2resreg(xmlfile)
    
    // lecture d'un fichier .xml contenant le resultat d'une regression et ecriture sous forme d'une structure 

    doc2=xmlRead(xmlfile)
    n_doc2=size(doc2.root.children); 
    if n_doc2 ~=8 then error ('8 fields expected') ; end

    for j=1:8;  
        data0=doc2.root.children(j).children(1).content;
        xdata.d=[];
        xdata.i=doc2.root.children(j).children(2).content;
        xdata.v=doc2.root.children(j).children(3).content;

        n=size(xdata.i,1);
        q=size(xdata.v,1);
        for i=1:n;
            line= strsplit(data0(i),',',q);
            line=strtod(line);
            xdata.d=[xdata.d;line'];    
        end
        xdata=div(xdata)
        
        select j
            case 1 then 
                res.err=[];
                res.err=xdata;
            case 2 then 
                res.ypredcv=[];
                res.ypredcv=xdata;
            case 3 then 
                res.b=[];
                res.b=xdata;
            case 4 then  
                res.scores=[]; 
                res.scores=xdata;
            case 5 then 
                res.loadings=[];
                res.loadings=xdata;
            case 6 then 
                res.x_mean=[];
                res.x_mean=xdata;
            case 7 then    
                res.y_mean=[];
                res.y_mean=xdata;
            case 8 then
                res.center=xdata.d;
        end
    end
        
        
        

 
 
 
    
endfunction
