<tool id="pcrregression" name="PCR Regression" version="0.0.1">
  <description> Principal component Regression using FACT-version 0.8 </description>
  <requirements>
    <requirement type="package" version="5.0.0">Scilab</requirement>
  </requirements>

  <command interpreter="python">
   pcrregression.py
      $Xdata
      $Ydata
      $ycolumn
      $crossval
      $latentvar
      $centering
      $rmsec_secv
      $b_coeffs
      $ypredcv
      $model
  </command>
  <inputs>
    <param name="Xdata"  format="tabular"           type="data"     label="Select X data" help="Dataset (n x p) containing the n spectra of p variables."/>
    <param name="Ydata"  format="tabular"           type="data"     label="Select y data" help="Dataset (n x q) containing the reference values."        />
    <param name="ycolumn" type="data_column"     data_ref="Ydata"  numerical="true"  multiple="False" use_header_names="true" force_select="true" label="Column of y data chosen for the calculation"/>
    <param name="crossval"   size="5" value="4"  type="integer"  min="2" max="20" label="Number of blocs for cross-validation"/>
    <param name="latentvar"  size="5" value="10"  type="integer"  min="2" max="50" label="Number of latent variables"                                                   />
    <param format="text" name="centering" type="select" label="Centering option">
      <option value="1" selected="true">yes</option>
      <option value="0">no</option>
    </param>
  </inputs>
  <outputs>
    <data format="input"   name="rmsec_secv"         type="data"     label="PCR on Y(${ycolumn.value_label}):rmsec_secv"    />
    <data format="input"   name="b_coeffs"         type="data"     label="PCR on Y(${ycolumn.value_label}):b-coeffs"           />
    <data format="input"   name="ypredcv"         type="data"     label="PCR on Y(${ycolumn.value_label}):ypredcv"            />
    <data format="xml"   name="model"         type="data"     label="PCR on Y(${ycolumn.value_label}):model"             />
  </outputs>
  <tests>
    <test>
      <param name="Xdata" value="Xnir.tabular"/>
      <param name="Ydata" value="Ynir.tabular"/>
      <param name="ycolumn" value="2" />
      <param name="crossval" value="4" />
      <param name="latentvar" value="10" />
      <param name="centering" value="1" />
      <output name="rmsec_secv" file="rmsec_secv.tabular"/>
      <output name="b_coeffs" file="b-coeffs.tabular"/>
      <output name="ypredcv" file="ypredcv.tabular"/>
      <output name="model" file="model.xml"/>
    </test>
  </tests>
<help>

.. class:: warningmark

**TIP:** If your data is not TAB delimited, use *Convert format data Tools-&gt;Convert*

.. class:: infomark

**Authors** Fabien Gogé (IRSTEA), Virginie Rossard (INRA), Eric Latrille (INRA), Jean-Michel Roger (IRSTEA), Jean-Claude Boulet (INRA)


---------------------------------------------------

===
PCR
===

-----------
Description
-----------


-----------
Input files
-----------

**X data**
Matrix of numeric data with headers (tabular format) and decimal separator ".".


+--------+-------+--------+
| Name   | 400   | 402    |
+========+=======+========+
|M102T645| 0.63  |0.64    |
+--------+-------+--------+
|M105T604| 0.5   |0.49    |
+--------+-------+--------+



**Y data**
Matrix of numeric data with headers (tabular format) and decimal separator ".".


+--------+-------+--------+
| Name   | Carbon| etc    |
+========+=======+========+
|M102T645| 2.10  | etc    |
+--------+-------+--------+
|M105T604| 50.4  | etc    |
+--------+-------+--------+

----------
Parameters
----------

**Column of y data chosen for the calculation**

Choice of property to predict

**Number of blocs for cross-validation**

Choice of the number of block for cross-validation

**Number of latent variables**

Choice of the number of latent variables

**centering**

If 0: no centering of the variables

------------
Output files
------------


Outfile1:**PCR Y(col N°):rmsec-secv values**

Values of Root Mean Square Error of Calibration (RMSEC) and Root Mean Square Error of Cross-Valibration (RMSECV) for each latent variable.

Outfile2:**PCR Y(col N°):b-coeffs**

Values of b-coefficients

Outfile3:**PCR Y(col N°):ypredcv**

Values of predicted values by cross-validation

Outfile4:**PCR Y(col N°):model**

xml file containing all the results of regression


</help>
</tool>
