function [saisir] = selectcol(saisir1,index)
%selectcol 		- creates a new data matrix with the selected columns
%function [X] = selectcol(X1,index)
%
%Input arguments
%===============
%X1: SAISIR matrix (n x p) 
%Index: vector of integer or of booleans
%
%Output argument
%===============
%X: matrix with n rows reduced to the selected variables
%
% %Typical example:
% %===============
%reduced=selectcol(DATA,[1 5 6]); %% selects the columns #1, #5, #6 and
%%builds the reduced matrix (with 3 columns) in "reduced"
%
%%See also: selectrow, deletecol, deleterow, appendcol, appendrow,
%appendcol1, appendrow1


saisir.d=saisir1.d(:,index);
saisir.i=saisir1.i;
saisir.v=saisir1.v(index,:);
