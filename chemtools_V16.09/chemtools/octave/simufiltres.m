function [x,flt]=simufiltres( sig, lam, f );
% [x,flt]=simufiltres( sig, lam, f );
% simulation de filtres passe-bande à partir de spectres de signal
%
%   sig : signal en entrée (individus en lignes, p colonnes)
%   lam : tableau des longueurs d'onde (1 ligne, p colonnes)
%   f : ensemble des filtres à simuler (2 colonnes, 1 filtre/ligne);
%
%   chaque filtre est caractérisé par :
%        1ère colonne : centre du filtre
%        2ème colonne : bande passante à mi hauteur
%
%   en retour, x contient
%           en colonnes les résultats des signaux traversant les filtres
%           en lignes les individus
%
%              flt contient les spectres des filtres
%
% l'équation d'un filtre, de centre L0, et de largeur à mi-hauteur B est :
%     y/ymax = exp( -4*ln(2)*( (L-L0)/B )^2 )


nf=size(f,1);
n=size(sig,1);

% génération des filtres
for i=1:nf
   flt(i,:)=exp( -( (lam-f(i,1))./f(i,2) ).^2 .* (4*log(2)) );
   s = sum(flt(i,:));
   if s > 0
       flt(i,:)=flt(i,:)./s;
   end;
end;

%calcul du signal
x=sig*flt';
