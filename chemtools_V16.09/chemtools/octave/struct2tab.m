function struct2tab(struct,filename)
%struct2tab 	  - Saves a struture file in a format compatible with tabular file
%function struct2tab(X,filename)
%
%Input arguments
%===============
%X: struct matrix to be saved
%filename: (string) name of the saved file
%
%Output argument
%===============
%none
%
%Transformq a struct file into a simple .CSV file with sep tabular and save it on disk
%The separator is ";"
%The extension '.csv' is added to the filename
%
%%Typical example:
%%===============
%struct2tab(data,'mydata');
%%Saves the struct matrix "data", under the name "data.csv", with ";" as separator
%%This file is read by tab
[n,p]=size(struct.d);

fid=fopen(filename,'w');
fprintf(fid,'%s\t','  ');
for col=1:p-1
	fprintf(fid,'%s\t',deblank(struct.v(col,:)));
end
fprintf(fid,'%s',deblank(struct.v(p,:)));
fprintf(fid,'\n');
for row=1:n
   %fprintf(fid,'$%s \t',struct.i(row,:));
   fprintf(fid,'%s \t',struct.i(row,:));
   fprintf(fid,'%0.5g \t',struct.d(row,1:p-1));
   fprintf(fid,'%0.5g',struct.d(row,p));
   fprintf(fid,'\n');
end
fclose(fid);

endfunction
