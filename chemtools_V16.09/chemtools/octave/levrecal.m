%# function [Levcal,VresXcal]=levrecal[T,A,ssq]
%#
%# But:         calculer les leverages et les r�sidus de chaque
%�chantillons
%# Entr�e:
%# T            Matrice des scores
%# A            Nombre de composantes PLS
%#OUTPUT:
%# Lev          Leverage de chaque point
%# VresX        R�sidus de chaque point

function [Levcal,VresXcal]=levrecal(T,A,ssq);

[n,m]=size(T);

%Calcul des T2Hot dans un mod�le � A composantes
    
   TT=(T).^2; %T carr�s ;
   s2=var(T); %variance de chaque composante     
   T2Hot=(TT./repmat(s2,n,1))*triu(ones(A)); %pour cumuler les T2i sur chaque composante
   %la derni�re colonne correspond � la somme cumul�e sur ncomp composantes des ti� pour chaque individu
   T2Hot=T2Hot(:,end); 
    
  %Calcul des leviers
    Levcal=(1/n)+T2Hot;
 
  %Calcul de la variance r�siduelle par individu
  VresXcal=var(ssq');
  
  %leverage
%Qt=var(T);
%Qt=ones(ncal,1)*Qt;
%DM=TT./Qt;
%h=1/ncal+sum(DM');