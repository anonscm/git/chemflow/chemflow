function x=glx_xml2xlist(xmlfile)

    % mise à jour du path
    addpath('~/chemflow/tools/chemtools/matlab/nway331')

    % lecture d'un fichier .xml contenant le resultat d'une regression et ecriture sous forme d'une structure

    doc=xmlRead(xmlfile);
    n_doc=max(size(doc.root.children));

    % Extraction of the first table
    x00=doc.root.children(1).content;
    n=size(x00,1);

    % Strings converted into double
    x0=[];
    for i=1:n;
        line= strsplit(x00(i),',');
        line=strtod(line);
        x0=[x0;line'];
    end

    [n,q]=size(x0);

    % Initialization of the output 3Dmatrix
    x=zeros(n,q,n_doc);
    %pause
    x(:,:,1)=x0;

    % Building the output 3Dmatrix
    for j=2:n_doc;
        xj0=doc.root.children(j).children(1).content;
        xj=[];
        for i=1:n;
            line= strsplit(xj0(i),',');
            line=strtod(line);
            xj=[xj;line'];
        end

        if size(xj)~=size(x0) then
           error ('For Parafac, all tables in the input .xml file should have the same dimension')
        else
           x(:,:,j)=xj;
        end
    end
