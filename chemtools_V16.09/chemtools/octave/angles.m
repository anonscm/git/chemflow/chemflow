function [ang] = angles(saisir1,saisir2)
%angles 			- Angles  between two tables
%function [ang] = cormap(X1,X2)
%
%Input arguments
%--------------
%X1 and X2: SAISIR matrix dimensioned n x p1 and n x p2 respectively
%
%Output argument:
%---------------
%ang: matrix of angles dimensioned p1 x p2)
%the tables must have the same number of rows
%An element ang.d(i,j) is the angles  between the column i of X1 and j of X2



[nrow1 ncol1]=size(saisir1.d);
[nrow2 ncol2]=size(saisir2.d);
if(nrow1~=nrow2); error('The number of rows must be equal');end

s1=diag(saisir1.d'*saisir1.d),
s2=diag(saisir2.d'*saisir2.d),

saisir1.d=saisir1.d *pinv(sqrt(diag(s1)));
saisir2.d=saisir2.d *pinv(sqrt(diag(s2)));

ang.d=saisir1.d'*saisir2.d;
ang.i=saisir1.v;
ang.v=saisir2.v;
