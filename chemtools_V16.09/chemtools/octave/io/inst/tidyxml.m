## Copyright (C) 2016 Philip Nienhuis
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{ostr} =} tidyxml (@var{istr})
## Remove characters (<32 >126) from text string or cell array @var{istr}
## and return the result in @var{ostr}.
##
## tidyxml is useful for cleaning up strings in xml that have been partly
## encoded as double-byte characters.  Such strings occur when dealing
## with spreadsheet programs reading/writing from/to XML-based formats
## and cannot be processed by Octave as Octave doesn't support unicode.
## For nested cell arrays tidyxml is called recursively and only processes
## cells containing text strings.
## @end deftypefn

## Author: Philip Nienhuis <prnienhuis@users.sf.net>
## Created: 2016-01-13

function [ostr] = tidyxml (istr="")

  if (iscell (istr))
    idx = find (cellfun (@ischar, istr));
    ostr = istr;
    ostr(idx) = cellfun (@tidyxml, istr(idx), "uni", 0);
  elseif (! ischar (istr))
    print_usage ();
  else
    istr = uint8 (istr);
    ostr = char (istr(find (istr > 31 & istr < 127)));
  endif

endfunction
