%# function [spectra]=image3d_2d[x]
%#
%# AIM:         Deplie une matrice 3D en une matrice 2D
%# INPUT:
%# x            Matrice 3D (hyperspectral image)
%#OUTPUT:
%# spectra      Matrice 2D (Spectra)

function [spectra]=image3d_2d[x]

[n,m,z]=size(x);
spectra=zeros(n*m,z);

for i=1:n
    for j=0:m-1
    x1=x(i,j+1,:);
    feat = reshape(x1,size(x1,1)*size(x1,2),size(x1,3));
    spectra(i+j,:)=feat;
    end
end
