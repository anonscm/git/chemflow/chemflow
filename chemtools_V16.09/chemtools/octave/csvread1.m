function [data,predata,postdata] = GParse (ftarget)

#declarations
datasheet=[];
precontours=[];
pstcontours=[];


#Load pre and post files
prefiles=glob ("*PRE*");
pstfiles=glob ("*POST*");
fname=char(prefiles(1));

#Make datasheet and enter coordinates for first 2 columns
coor=csvread(fname,1,0);coor=coor(:,1:2);
datasheet=[datasheet,coor];

#create data files for contourbatch function
precontours=[precontours,coor];
pstcontours=[pstcontours,coor];


for i=1:size(prefiles,1)
  fname=char(prefiles(i));
  zpre=csvread(fname,"C2:C70");
  fname=char(pstfiles(i));
  zpst=csvread(fname,"C2:C70");
  etarget=zpre-ftarget;
  eactual=zpre-zpst;
  error=eactual-etarget;
  datasheet=[datasheet,zpre,zpst,target,eactual,eerror];
  precontours=[precontours,zpre];
  pstcontours=[pstcontours,zpst];
endfor

stats=[mean(datasheet);min(datasheet);max(datasheet);range(datasheet);std(datasheet)];
datasheet=[datasheet;stats];
data=datasheet;
endfuncion
