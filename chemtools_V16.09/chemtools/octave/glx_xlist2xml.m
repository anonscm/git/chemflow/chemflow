function glx_xlist2xml(x,xmlfile)

    % mise à jour du path
    addpath('~/chemflow/tools/chemtools/matlab/nway331')


    % x est une liste de q matrices
    doc = xmlread(xmlfile);
pause
    root=xmlElement(doc,"root");
    doc.root=root;


    % Initialisation
    n=length(x);


    % construction de la structure
    for i=1:q;
        name='plan'+string(i)
        plani=x(:,:,i);

        % mise au format csv: separation par ','
        % note: doit pouvoir bien s'améliorer!
        xd=[];
        for j=1:n;
            xdj=[];
            for k=1:p-1;
                xdj=xdj + string(plani(j,k)) +',';
            end
            xdj=xdj+string(plani(j,p));
            xd=[xd;xdj];
        end

        %pause
        execstr(msprintf("%s=xmlElement(doc,name)",name))
        %execstr("xmlAppend(doc,name);");
        execstr(msprintf('doc.root.children(i)=%s;',name));

        doc.root.children(i).content=string(xd);

    end

    xmlWrite(doc,xmlfile,%t)

    xmlDelete('all')
