function handle=angles_plot(basis,col1, col2, varargin);
%angles_plot       - Draw a angles between basis and tables
%function handle=angles_plot(basis,col1,col2, X1,X2, ...);
%
% Input arguments:
% ==============
% basis:                  - ORTHOGONAL basis obtained by multidimensional analysis
% col1 and col2:           - Indices (ranks) of the basis to be plotted (integer number)
% X, X2, ...    - Arbitrary number of tables giving the variables to be plotted
% The number of rows in the basis and other tables must be identical
%
%The function displays the angles circle, with a different color for
%each table in X1, X2 ...
%A dotted line gives the level of 50% explained variable.
%If the input argument "basis" have non-orthogonal columns the graph is normally incorrect
%and a warning message is displayed.

charsize=10;
couleur=[0 0 0; 0 0 1; 0 0.7 0; 1 0 0; 0.5 0.5 0; 0.5 0 0.5; 0 0.5 0.5 ; 0.25 0.25 0.25 ; 0.5 0 0; 0 0.5 0; 0 0.5 0; 0.1 0.2 0.3; 0.3 0.2 0.1; 0.5 0.5 0.8; 0.1 0.8 0.1 ];

[n,p]=size(basis.d);

axis square;
t=0:pi/20:2*pi;
plot(sin(t),cos(t))
hold on;
cst=sqrt(0.5);%% drawing 50% variance explained
h=plot(sin(t)*cst,cos(t)*cst);
set(h,'LineStyle','--');
line([-1 1],[0 0]);
line([0 0],[-1 1]);
axis square
xlabel(basis.v(col1,:));
ylabel(basis.v(col2,:));
[a,ntable]=size(varargin);
basis=selectcol(basis,[col1 col2]);
%%basis
cor=angles(basis,basis);
aux=cor.d(1,2);
if((aux*aux)>0.05)
    disp([ 'Determination coefficient between the 2 basis = ' num2str(aux*aux)]);
    disp('Warning : the scores are not sufficiently orthogonal to allow a correct representation');
end
for i=1:ntable
     k=mod(i,15)+1;
     this_color=couleur(k,:);
     this_table=(varargin{i});
     cor=angles(this_table,basis);
     [n,p]=size(cor.d);
     for j=1:n
         h1=text(cor.d(j,1),cor.d(j,2),cor.i(j,:),'FontSize',charsize,'Color',this_color);
         set(h1,'Interpreter','none');
     end
end

hold off;
handle=h;
